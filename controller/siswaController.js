const Siswa = require("../models/siswa")
const help = require("../utils/helpers")
controller = {}

controller.getAll = async (req, res) => {
  try {
    let getAllSiswa = await Siswa.findAll();

    let result = getAllSiswa
    let message = "Success"

    if (getAllSiswa.length == 0) {
      result = 0
      message = "Data tidak tersedia"
    } 

    return help.response(res, 200, message, result);
  } catch {
    console.log(err);
    return help.responseErr(res, 500, "Error", err.message);
  }
};

controller.getOne = async (req, res) => {
  let id = req.params.id

  try {
    let getOneSiswa = await Siswa.findOne({
      where: {
        id: id
      }
    });
    let result = getOneSiswa
    let message = "Success"

    if (getOneSiswa == null) {
      result = 0
      message = "Data tidak tersedia"
    } 

    return help.response(res, 200, message, result);
  } catch {
    console.log(err);
    return help.responseErr(res, 500, "Error", err.message);
  }
}

controller.create = async (req, res) => {
  try {
    let createSiswa = await Siswa.create(req.body)
    console.log(createSiswa)

    return help.response(res, 200, "")
  } catch (err) {
    console.log(err)
    return help.responseErr(res, 500, "Error", err.message)
  }
}

controller.update = async (req, res) => {
  let message = "Success"
  try {
    let updateSiswa = await Siswa.update(req.body, {
      where: {
        id: req.params.id
      }
    })

    if (updateSiswa[0] == 0) {
      message = "Gagal edit data siswa"
      return help.responseErr(res, 400, message, "")
    }

    return help.response(res, 200, message, "")
  } catch(err) {
    console.log(err)
    return help.responseErr(res, 500, "Error", err.message)
  }
}
controller.delete = async (req, res) => {
  let message = "Success"
  try {
    let isExist = await Siswa.findOne({where: {id: req.params.id}})
    if (isExist == null) {
      message = "Data tidak ditemukan"
      return help.responseErr(res, 400, message)
    }

    await Siswa.destroy({where: {id: req.params.id}})

    return help.response(res, 200, message)
  } catch (err) {
    console.log(err)
    return help.responseErr(res, 500, "Error", err.message)
  }
}

module.exports = controller
