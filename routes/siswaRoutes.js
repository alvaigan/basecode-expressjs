const siswaController = require("../controller/siswaController")

module.exports = (router) => {
  router.get("/siswa", siswaController.getAll)
  router.get("/siswa/:id/data", siswaController.getOne)
  router.post("/siswa/add", siswaController.create)
  router.put("/siswa/:id/edit", siswaController.update)
  router.delete("/siswa/:id/delete", siswaController.delete)
}
