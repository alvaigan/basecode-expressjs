const Sequelize = require("sequelize");
const sequelize = require("../config/db");

let Siswa = sequelize.define(
  "siswa",
  {
    id: {
      field: "id",
      autoIncrement: true,
      type: Sequelize.INTEGER,
      primaryKey: true,
      allowNull: false,
    },
    nik: {
      field: "nik",
      allowNull: false,
      type: Sequelize.STRING,
    },
    nama: {
      field: "nama",
      allowNull: false,
      type: Sequelize.STRING,
    },
    kelas: {
      field: "kelas",
      allowNull: false,
      type: Sequelize.STRING,
    },
  },
  {
    tableName: "t_siswa",
    freezeTableName: true,
  }
);

module.exports = Siswa;
