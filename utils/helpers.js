const response = (res, code, message, data, extra = {}) => {
  return res.status(code).json({
    code: code,
    message: message,
    data: data,
    extra: extra,
  });
};

const responseErr = (res, code, message, data) => {
  return res.status(code).json({
    code: code,
    message: message || "error",
    errors: data,
  });
};

module.exports = {
  response,
  responseErr
}
