require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");
const port = process.env.PORT;
const app = express();
const router = express.Router();

app.use(
  cors(),
  express.static("public"),
  bodyParser.json(),
  bodyParser.urlencoded({ extended: true })
)

global.appRoot = path.resolve(__dirname);

const siswaRoutes = require("./routes/siswaRoutes")

app.use("/", router)

siswaRoutes(router)

app.listen(port, () => {
  console.log("Server berjalan pada port: "+ port)
})
